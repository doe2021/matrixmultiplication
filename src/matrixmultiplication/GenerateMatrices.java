/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixmultiplication;

import com.Ostermiller.util.CSVPrinter;
import java.io.FileOutputStream;

/**
 *
 * @author l3cla
 */
public class GenerateMatrices {
    
    
    public static void main(String[]args)throws Exception{
        
        int row = 1500;
        int column = 1700;
        int inner = 1800;
        
        int half = (int)Math.sqrt(Integer.MAX_VALUE);
        int Max = half*2;
        String[][] matrixA =new String[row][inner];
        String [][]matrixB = new String[inner][column];
        for (int i = 0;i< row;++i){
            for (int j = 0;j< inner;++j){
                matrixA[i][j]= String.valueOf((int)((Math.random()*Max)-half));
            }
        }
        for (int i = 0;i< inner;++i){
            for (int j = 0;j< column;++j){
                matrixB[i][j]= String.valueOf((int)(Math.random()*Max)-half);
            }
        }
        
        CSVPrinter printer = new CSVPrinter(new FileOutputStream("matrices/Batch3/matrixA.csv"));
        printer.println(matrixA);
        printer.close();
        
        printer = new CSVPrinter(new FileOutputStream("matrices/Batch3/matrixB.csv"));
        printer.println(matrixB);
        printer.close();
    }
    
}
