/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixmultiplication;

import com.Ostermiller.util.CSVParser;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class DoubleMatrix {

    private final int row;
    private final int column;

    private final int inner;
    private final double[][] matrixA;
    private final double[][] matrixB;
    private final AtomicInteger totalEntries;
    private final AtomicInteger totalThreads;
    private final long initTime;

    private final boolean print = false;
    public double[][] multiplied;

    class Multiplier extends Thread {

        @Override
        public void run() {
            int currentEntry = totalEntries.decrementAndGet();
            while (currentEntry >= 0) {
                int currentRow = currentEntry / column;
                int currentColumn = currentEntry % column;
                double data = 0;
                for (int i = 0; i < inner; ++i) {
                    data += matrixA[currentRow][i] * matrixB[i][currentColumn];

                }

                multiplied[currentRow][currentColumn] = data;
                currentEntry = totalEntries.decrementAndGet();
            }

            int current = totalThreads.decrementAndGet();

            if (current == 0) {

                System.out.println("Total time " + (System.currentTimeMillis() - initTime));
                if (print) {
                    for (int i = 0; i < row; ++i) {
                        for (int j = 0; j < column; ++j) {
                            System.out.print(multiplied[i][j] + " ");
                        }
                        System.out.println();
                    }
                }
            }
        }
    }

    private double[][] translate(String[][] text) {
        int mRow = text.length;
        int mColumn = text[0].length;
        double[][] matrix = new double[mRow][mColumn];
        for (int i = 0; i < mRow; ++i) {
            for (int j = 0; j < mColumn; ++j) {
                matrix[i][j] = Double.parseDouble(text[i][j]);
            }
        }
        return matrix;
    }

    public DoubleMatrix(int threads, String pathA, String pathB) throws Exception {
        totalThreads = new AtomicInteger(threads);
        matrixA = translate(CSVParser.parse(new FileReader(pathA)));
        matrixB = translate(CSVParser.parse(new FileReader(pathB)));

        row = matrixA.length;
        inner = matrixB.length;
        column = matrixB[0].length;
        totalEntries = new AtomicInteger(row * column);
        multiplied = new double[row][column];
        ArrayList<Multiplier> multipliers = new ArrayList<>();
        for (int i = 0; i < threads; ++i) {
            multipliers.add(new Multiplier());
        }
        initTime = System.currentTimeMillis();
        for (Multiplier mp : multipliers) {
            mp.start();
        }
    }
}
