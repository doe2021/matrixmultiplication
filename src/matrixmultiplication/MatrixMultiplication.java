/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixmultiplication;

/**
 *
 * @author l3cla
 */
public class MatrixMultiplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        if ("int".equals(args[0])){
            new IntMatrix(Integer.parseInt(args[1]), args[2], args[3]);
        }else if ("double".equals(args[0])){
            new DoubleMatrix(Integer.parseInt(args[1]), args[2], args[3]);
        }
    }
    
}
